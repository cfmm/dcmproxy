//
//  AppDelegate.swift
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-03-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

import Cocoa


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {


   func applicationDidFinishLaunching(_ aNotification: Notification) {
      // Insert code here to initialize your application
   }

   func applicationWillTerminate(_ aNotification: Notification) {
      // Insert code here to tear down your application
   }

}

