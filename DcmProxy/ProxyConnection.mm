//
//  DicomProxy.m
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-12.
//  Copyright © 2017 CFMM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <vector>
#import "ProxyConnection.h"
#import "ProxyServer.h"
#import "PDU.hpp"

static const size_t BUFFER_SIZE = 65536;

@implementation ProxyConnection {
    dispatch_queue_t connectionQueue;
    GCDAsyncSocket *clientSocket;
    GCDAsyncSocket *serverSocket;
    NSMutableData *clientData;
    NSMutableData *serverData;
    NSTimeInterval timeOut;
    ProxyServer *parentServer;
}

enum MSG {
    CLIENT_MSG,
    SERVER_MSG,
    CLIENT_PDU_TYPE,
    CLIENT_ASSOCIATION
};

- (id) initWithSocket:(GCDAsyncSocket *) socket
           fromServer:(ProxyServer *) server
{
    if((self = [super init]))
    {
        timeOut = NSTimeInterval(-1);
        
        clientSocket = socket;
        [clientSocket setDelegate:self];
        
        parentServer = server;
        serverSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:[server queue]];
        
        clientData = [[NSMutableData alloc] initWithCapacity:BUFFER_SIZE];
        serverData = [[NSMutableData alloc] initWithCapacity:BUFFER_SIZE];
        
        NSError *error;
        if (![serverSocket connectToHost:[server dicomServer] onPort:[server dicomPort] withTimeout:timeOut error:&error])
        {
            NSLog(@"Connection error to %@:%i: %@", [server dicomServer], [server dicomPort], error);
            [clientSocket disconnect];
            self = nil;
        }
    }
    return self;
}

- (void) stop;
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:stop");
#endif
    [clientSocket disconnectAfterReadingAndWriting];
}

- (void) socket:(GCDAsyncSocket *)sock didConnectToHost:(nonnull NSString *)host port:(uint16_t)port
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:socket:(%@) didConnecToHost:(%@) port:(%i)", sock, host, port);
#endif
    // Enable TLS
    NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [settings setObject:[NSNumber numberWithBool:YES]
                 forKey:(NSString *)GCDAsyncSocketManuallyEvaluateTrust];
    
    [serverSocket startTLS:settings];
}

- (void) socket:(GCDAsyncSocket *)sock didReceiveTrust:(SecTrustRef)trust completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler
{
   SecTrustResultType trustResult;
   
   SecTrustEvaluate(trust, &trustResult);
   if ((trustResult == kSecTrustResultProceed) ||
       (trustResult == kSecTrustResultUnspecified))
   {
      if (completionHandler) completionHandler(YES);
      return;
   }
   else if (trustResult == kSecTrustResultRecoverableTrustFailure)
   {
      // No recovery attempted. Could check to expiry and then ignore expired certificates.
   }

   if (completionHandler) completionHandler(NO);
}

- (void) socketDidSecure:(GCDAsyncSocket *)sock
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:socketDidSecure:(%@)", sock);
    NSLog(@"clientSocket (%@) delegate (%@) self (%@)", clientSocket, [clientSocket delegate], self);
#endif
    // Start processing the client data
    // Read in the first client PDU type and length
    [clientSocket readDataToLength:6 withTimeout:timeOut buffer:clientData bufferOffset: 0 tag:CLIENT_PDU_TYPE];
}

- (void) startPassing
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:startPassing");
#endif
    // Pass happens on a separate queue
    connectionQueue = dispatch_queue_create("connectionQueue", NULL);
    [clientSocket setDelegateQueue:connectionQueue];
    [serverSocket setDelegateQueue:connectionQueue];
    
    [serverSocket readDataWithTimeout:timeOut buffer:serverData bufferOffset:0 tag:SERVER_MSG];
    [serverSocket writeData:clientData withTimeout:timeOut tag:SERVER_MSG];
}

- (void) socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:socket:(%@) didReadData:(%@) didReadData:(%ld)", sock, data, tag);
#endif
    switch (tag)
    {
        case CLIENT_PDU_TYPE:
        {
            const char *pData = reinterpret_cast<const char *>([data bytes]);
            
            // Check the type
            if (pData[0] == 0x01) {
                // This is an association request
                
                // Get the length
                size_t length = ntohl(*(reinterpret_cast<const uint32_t *>(&(pData[2]))));
                
                // Read the rest of the client association request
                // This read is special, because the PDU will be modified from serverData into clientData
                [clientSocket readDataToLength:length withTimeout:timeOut buffer:serverData bufferOffset:0 tag:CLIENT_ASSOCIATION];
            }
            else
            {
                // Unknown type, send it on to the server for processing
                [self startPassing];
            }
            return;
        }
        case CLIENT_ASSOCIATION:
        {
            std::vector<char> userdata;
            {
                NSMutableString * username = [[NSMutableString alloc] init];
                NSMutableString * password = [[NSMutableString alloc] init];
                [parentServer getUsername:username andPassword:password];
                createUserdata(userdata, std::string([username UTF8String]), std::string([password UTF8String]));
            }
            
            // Allocate space for the modified client PDU
            // The original client PDU was read into serverData
            // The maximum length of the PDU to the server
            // Requires 6 bytes for PDU type and length
            // May need 4 bytes for other fields type and length
            // Requires userdata.size() bytes for user information
            [clientData setLength:[data length] + 10 + userdata.size()];
            
            // Create the new PDU with the user identity and truncate the data to the required length
            [clientData setLength:editPDU((char *)[clientData mutableBytes], (const char *)[data bytes], [data length], userdata.data(), userdata.size())];
            
            [self startPassing];
            return;
        }
        case SERVER_MSG:
            // Read data from server
            // Write data to client
            [clientSocket writeData:data withTimeout:timeOut tag:CLIENT_MSG];
            break;
        case CLIENT_MSG:
            // Read data from client
            // Write data to server
            [serverSocket writeData:data withTimeout:timeOut tag:SERVER_MSG];
            break;
    }
}

- (void) socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:socket:(%@) didWriteDataWithTag:(%ld)", sock, tag);
#endif
    if (tag == CLIENT_MSG)
    {
        // Wrote data to client
        // Read in the next message from the server
        [serverSocket readDataWithTimeout:timeOut buffer:serverData bufferOffset:0 tag:SERVER_MSG];
    }
    else
    {
        // Wrote data to server
        // Read in the next message from the client
        [clientSocket readDataWithTimeout:timeOut buffer:clientData bufferOffset:0 tag:CLIENT_MSG];
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
#ifdef DEBUG
    NSLog(@"ProxyConnection:socketDidDisconnect:(%@) withError:(%@)", sock, err);
#endif
    NSString *type;

    if (sock == clientSocket)
    {
        type = @"client";
        
        // Client has disconnected
        if ([serverSocket isConnected])
        {
            // Disconnect from DICOM Server after all pending writes are completed
            [serverSocket disconnectAfterWriting];
        }
        else
        {
            // Report connection terminated
            [parentServer connectionFinished:self];
        }
    }
    else
    {
        type = @"server";
        // Server has disconnected
        if ([clientSocket isConnected])
        {
            // Disconnect from the DICOM Client after all pending writes are completed
            [clientSocket disconnectAfterWriting];
        }
        else
        {
            // Report connection terminated
            [parentServer connectionFinished:self];
        }
    }
    
    if (err)
    {
        NSLog(@"Disconnect for %@ with error: %@", type, err);
    }
}

@end


