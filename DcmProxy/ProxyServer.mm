//
//  ProxyService.m
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-07-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProxyServer.h"
#import "KeyChainServerAccess.hpp"
#import "ProxyConnection.h"

@implementation ProxyServer {
    GCDAsyncSocket *listenSocket;
    dispatch_queue_t listenQueue;
    NSMutableArray *connections;
    
    KeyChainServerAccess    *access;
    NSString *bAddress;
    uint16_t bPort;
    NSString *dServer;
    uint16_t dPort;
    NSString *dUsername;
    
    size_t maximumRetries;
}

- (id) init
{
    if((self = [super init]))
    {
        listenQueue = dispatch_queue_create("listenQueue", NULL);
        
        listenSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:listenQueue];
        
        // Setup an array to store all accepted client connections
        connections = [[NSMutableArray alloc] initWithCapacity:1];
        
        access = nil;
    }
    return self;
}

- (void) dealloc
{
#ifdef DEBUG
    NSLog(@"ProxyService:dealloc");
#endif
    [self stop];

    if (access != nil)
        delete access;
}

- (dispatch_queue_t) queue
{
    return listenQueue;
}

- (void) getUsername:(NSMutableString *)username andPassword:(NSMutableString *)password
{

#ifdef DEBUG
    NSLog(@"ProxyService:getUsername:andPassword");
#endif
    std::string tmpPassword;
    access->getPassword(tmpPassword);

    if (nil != username)
       [username setString:[NSString stringWithUTF8String:access->username().c_str()]];
    if (nil != password)
       [password setString:[NSString stringWithUTF8String:tmpPassword.c_str()]];
}

- (void) listenOnInterface:(NSString *)bindAddress
                    onPort:(NSString *)bindPort
                  toServer:(NSString *)server
                    onPort:(NSString *)port
               forUsername:(NSString *)username
              withPassword:(NSString *)password
{
#ifdef DEBUG
    NSLog(@"ProxyService:listenOnInterface:(%@) onPort:(%@) toServer:(%@) onPort:(%@) forUsername:(%@) withPassword:(*)", bindAddress, bindPort, server, port, username);
#endif
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    
    bAddress = bindAddress;
    bPort = [[formatter numberFromString:bindPort] unsignedShortValue];
    
    if (server.length > 0)
        dServer = server;
    if (port.length > 0)
        dPort = [[formatter numberFromString:port] unsignedShortValue];
    if (username.length > 0)
        dUsername = username;
    
    BOOL configure = true;
    
    if (access == nil)
    {
        access = new KeyChainServerAccess([dServer UTF8String],
                                          dPort,
                                          [dUsername UTF8String],
                                          "DcmProxy");
        
        configure = password != nil;
        
    }
    
    if (configure)
    {
        access->configure([dServer UTF8String],
                          dPort,
                          [dUsername UTF8String],
                          [password UTF8String]);
    }
    
    // For listening sockets isConnected is always false
    // isDisconnected is false when not listening
    if ( ![listenSocket isDisconnected] && ([listenSocket localPort] != bPort || ![bAddress isEqualToString:[listenSocket localHost]] ) )
    {
        NSLog(@"Disconnecting from %@:%i", [listenSocket localHost], [listenSocket localPort]);
        [listenSocket setDelegate:nil];
        [listenSocket disconnect];
        [listenSocket setDelegate:self];
    }
    
    if ([listenSocket isDisconnected])
    {
        NSError *error = nil;
        if ( ![listenSocket acceptOnInterface:bAddress port:bPort error:&error] )
        {
            NSLog(@"Listen error: %@", error);
        }
        
        NSLog(@"Listening on %@:%i", bAddress, bPort);
    }
}

- (NSString *) dicomServer
{
#ifdef DEBUG
    NSLog(@"ProxyService:dicomServer");
#endif
    return dServer;
}

- (uint16_t) dicomPort
{
#ifdef DEBUG
    NSLog(@"ProxyService:dicomPort");
#endif
    return dPort;
}

- (void) socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
#ifdef DEBUG
    NSLog(@"ProxyService:socket:(%@) didAcceptNewSocket:(%@)", sock, newSocket);
#endif
    ProxyConnection *proxyConnection = [[ProxyConnection alloc] initWithSocket: newSocket
                                                                    fromServer: self];

    if (proxyConnection)
    {
        @synchronized(connections)
        {
            [connections addObject:proxyConnection];
        }
    }
}

- (void) stop
{
#ifdef DEBUG
    NSLog(@"ProxyService:stop");
#endif
    [listenSocket disconnect];
    
    // Close down all the existing connections
    for (ProxyConnection *connection in connections)
    {
        [connection stop];
    }
}

- (void) connectionFinished:(ProxyConnection *)connection
{
#ifdef DEBUG
    NSLog(@"ProxyService:connectionFinished:(%@)", connection);
#endif
    @synchronized(connections)
    {
        [connections removeObject:connection];
    }
}

@end


