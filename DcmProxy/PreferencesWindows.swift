//
//  PreferencesWindows.swift
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-03-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

import Cocoa

protocol PreferencesWindowDelegate {
   func preferencesDidUpdate(password: String)
}

class PreferencesWindow: NSWindowController, NSWindowDelegate {

   var delegate: PreferencesWindowDelegate?

   @IBOutlet weak var passwordTextField: NSSecureTextField!
   @IBOutlet weak var usernameTextField: NSTextField!
   @IBOutlet weak var severTextField: NSTextField!
   @IBOutlet weak var portTextField: NSTextField!
   @IBOutlet weak var bindTextField: NSTextField!
   @IBOutlet weak var bindPortTextField: NSTextField!

   @IBAction func okClicked(_ sender: NSButton) {
      self.window?.close()
   }

   override var windowNibName: NSNib.Name? {
    return "PreferencesWindow"
   }

   func windowWillClose(_ notification: Notification) {
      let defaults = UserDefaults.standard
      defaults.setValue(usernameTextField.stringValue, forKey: "username")
      defaults.setValue(severTextField.stringValue, forKey: "server")
      defaults.setValue(portTextField.stringValue, forKey: "port")
      defaults.setValue(bindTextField.stringValue, forKey: "bind")
      defaults.setValue(bindPortTextField.stringValue, forKey: "bind-port")

      delegate?.preferencesDidUpdate(password: passwordTextField.stringValue)

      // Clear the password string
      passwordTextField.stringValue = ""
   }

   override func windowDidLoad() {
      super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
      self.window?.center()
      self.window?.makeKeyAndOrderFront(nil)
      NSApp.activate(ignoringOtherApps: true)

      let defaults = UserDefaults.standard
      usernameTextField.stringValue = defaults.string(forKey: "username") ?? "sample"
      severTextField.stringValue = defaults.string(forKey: "server") ?? "dicom.server.com"
      portTextField.stringValue = defaults.string(forKey: "port") ?? "11112"
      bindTextField.stringValue = defaults.string(forKey: "bind") ?? "127.0.0.1"
      bindPortTextField.stringValue = defaults.string(forKey: "bind-port") ?? "11112"
   }

}
