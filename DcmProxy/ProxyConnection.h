//
//  DicomProxy.h
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-12.
//  Copyright © 2017 CFMM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GCDAsyncSocket.h>

@class ProxyServer;

@interface ProxyConnection : NSObject<GCDAsyncSocketDelegate> {
}

- (id) initWithSocket:(GCDAsyncSocket *) socket
           fromServer:(ProxyServer *) server;

- (void) stop;

@end

