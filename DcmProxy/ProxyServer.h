//
//  ProxyService.h
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-07-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GCDAsyncSocket.h>

@class ProxyConnection;

@interface ProxyServer : NSObject<GCDAsyncSocketDelegate> {
}

- (void) listenOnInterface:(NSString *)bindAddress
                    onPort:(NSString *)bindPort
                  toServer:(NSString *)server
                    onPort:(NSString *)port
               forUsername:(NSString *)username
              withPassword:(NSString *)password;

- (void) stop;

- (void) getUsername:(NSMutableString *)username
         andPassword:(NSMutableString *)password;

- (NSString *) dicomServer;

- (uint16_t) dicomPort;

- (dispatch_queue_t) queue;

- (void) connectionFinished:(ProxyConnection *)connection;

@end
