//
//  StatusMenuController.swift
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-03-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

import Cocoa

class StatusMenuController: NSObject, PreferencesWindowDelegate {
   var preferencesWindow: PreferencesWindow!

   @IBOutlet weak var statusMenu: NSMenu!

   let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
   let proxyServer = ProxyServer()

   override func awakeFromNib() {
      let icon = NSImage(named: "statusIcon")
      icon?.isTemplate = true // best for dark mode
      statusItem.image = icon
      statusItem.menu = statusMenu

      preferencesWindow = PreferencesWindow()
      preferencesWindow.delegate = self

      let defaults = UserDefaults.standard
      proxyServer.listen(onInterface: defaults.string(forKey: "bind") ?? "127.0.0.1",
                         onPort: defaults.string(forKey: "bind-port") ?? "11112",
                         toServer: defaults.string(forKey: "server") ?? "dicom.server.com",
                         onPort: defaults.string(forKey: "port") ?? "11112",
                         forUsername: defaults.string(forKey: "username") ?? "user",
                         withPassword: nil)
   }

   deinit {
      proxyServer.stop()
   }

   @IBAction func quitClicked(_ sender: NSMenuItem) {
      NSApplication.shared.terminate(self)
   }

   @IBAction func preferencesClicked(_ sender: NSMenuItem) {
      NSApp.activate(ignoringOtherApps: true)
      preferencesWindow.window?.makeKeyAndOrderFront(self)
      preferencesWindow.showWindow(nil)
   }

   func preferencesDidUpdate(password: String) {
       let defaults = UserDefaults.standard
       proxyServer.listen(onInterface: defaults.string(forKey: "bind"),
                          onPort: defaults.string(forKey: "bind-port"),
                          toServer: defaults.string(forKey: "server"),
                          onPort: defaults.string(forKey: "port"),
                          forUsername: defaults.string(forKey: "username"),
                          withPassword: password)
   }
}
