//
//  DicomProxyConnection.hpp
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#ifndef DicomProxyConnection_hpp
#define DicomProxyConnection_hpp

#ifndef WIN32
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#pragma clang diagnostic ignored "-Wconversion"
#endif

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ssl.hpp>

#ifndef WIN32
#pragma clang diagnostic pop
#endif

#include "ServerAccess.hpp"

#define BUFFERSIZE 65536


namespace ip = boost::asio::ip;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<ip::tcp::socket> ssl_stream;


class DicomProxyConnection
: public boost::enable_shared_from_this<DicomProxyConnection>
{
public:
   typedef boost::shared_ptr<DicomProxyConnection> pointer;

   static pointer create(boost::asio::io_service& io_service, ssl::context& ctx, const ServerAccess &access)
   {
      return pointer(new DicomProxyConnection(io_service, ctx, access));
   }

   ip::tcp::socket& socket()
   {
      return m_client;
   }

   ssl_stream& server()
   {
      return m_server;
   }

   void start();

   unsigned int maximum_retries()
   {
      return m_maximum_retries;
   }

   void maximum_retries(unsigned int maximum_retries)
   {
      m_maximum_retries = maximum_retries;
   }
private:
   DicomProxyConnection(boost::asio::io_service& io_service, ssl::context& ctx, const ServerAccess &access)
   : m_client(io_service)
   , m_server(io_service, ctx)
   , m_resolver(io_service)
   , m_access(access)
   , m_client_buffer(BUFFERSIZE)
   , m_server_buffer(BUFFERSIZE)
   , m_maximum_retries(5)
   {}

   void handle_connect(const boost::system::error_code& error,
                       ip::tcp::resolver::iterator iterator);

   void handle_client_read(const boost::system::error_code& error,
                           size_t bytes_transferred);

   void handle_client_write(const boost::system::error_code& error,
                            size_t /*bytes_transferred*/);

   void handle_server_read(const boost::system::error_code& error,
                           size_t bytes_transferred);

   void handle_server_write(const boost::system::error_code& error,
                            size_t /*bytes_transferred*/);

   ip::tcp::resolver m_resolver;
   ip::tcp::socket m_client;
   ssl_stream m_server;

   const ServerAccess &m_access;

   std::vector<char> m_client_buffer;
   std::vector<char> m_server_buffer;

   unsigned int m_retries;
   unsigned int m_maximum_retries;
};

#endif /* DicomProxyConnection_hpp */
