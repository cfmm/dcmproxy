//
//  PasswordVaultServerAccess.hpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-20.
//  Copyright © 2017 CFMM. All rights reserved.
//

#ifndef PasswordVaultServerAccess_hpp
#define PasswordVaultServerAccess_hpp

#include "ServerAccess.hpp"

class PasswordVaultServerAccess : public ServerAccess
{
public:
   PasswordVaultServerAccess(std::string const &hostname,
                             unsigned short port,
                             std::string const &username,
                             std::string const&resource,
                             void (*log)(const std::string &msg)=nullptr)
   : ServerAccess(hostname, port, username, log)
   , m_resource(resource)
   {}

   virtual void query();

protected:
   virtual std::string retrieve_password() const;
   virtual void set_password(std::string const &password);

private:
   bool retrieve(std::string &password) const;
   std::string m_resource;

};

#endif /* PasswordVaultServerAccess_hpp */
