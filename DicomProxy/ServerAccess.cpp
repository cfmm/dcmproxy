//
//  SeverAccess.cpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-13.
//  Copyright © 2017 CFMM. All rights reserved.
//

#include "ServerAccess.hpp"
#include <mutex>

std::mutex g_mutex;


bool ServerAccess::getPassword(std::string &password) const
{
   std::lock_guard<std::mutex> guard(g_mutex);
   return retrieve_password(password);
}

const std::string &ServerAccess::hostname() const
{
   std::lock_guard<std::mutex> guard(g_mutex);
   return m_hostname;
}

const std::string &ServerAccess::username() const
{
   std::lock_guard<std::mutex> guard(g_mutex);
   return m_username;
}

unsigned short ServerAccess::port() const
{
   std::lock_guard<std::mutex> guard(g_mutex);
   return m_port;
}

void ServerAccess::configure(const std::string &hostname, unsigned short port, const std::string &username, const std::string &password)
{
   std::lock_guard<std::mutex> guard(g_mutex);
   m_hostname = hostname;
   m_port = port;
   m_username = username;
   if (!password.empty())
   {
      set_password(password);
   }
}

void ServerAccess::password(const std::string &password)
{
   if (!password.empty())
   {
      std::lock_guard<std::mutex> guard(g_mutex);
      set_password(password);
   }
}

bool ServerAccess::query()
{
   std::string password;
   return retrieve_password(password);
}


