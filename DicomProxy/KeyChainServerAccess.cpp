//
//  PasswordAccess.cpp
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#include "KeyChainServerAccess.hpp"
#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <sstream>

OSStatus KeyChainServerAccess::retrieve(std::string &password) const
{
   uint32_t passwordLength;
   void *passwordData;

   OSStatus status = SecKeychainFindGenericPassword(
                                                    NULL,                                       // default keychain
                                                    static_cast<uint32_t>(m_service.length()),  // length of service name
                                                    m_service.data(),                           // service name
                                                    static_cast<uint32_t>(m_username.length()), // length of account name
                                                    m_username.data(),                          // account name
                                                    &passwordLength,                            // length of password
                                                    &passwordData,                               // pointer to password data
                                                    &m_itemRef                                  // the item reference
                                                    );
   if (status == noErr)
   {
      password.assign(reinterpret_cast<char *>(passwordData), passwordLength);
      SecKeychainItemFreeContent(NULL, passwordData);
      return status;
   }
   else if (status == errSecItemNotFound)
   {
      return status;
   }

   std::stringstream ss;
   ss << "Error finding password: " << status;
   log(ss.str());
    
   return status;
}

bool KeyChainServerAccess::retrieve_password(std::string &password) const
{
   OSStatus status = retrieve(password);

   if (status == noErr)
   {
      return true;
   }

   return false;
}

void KeyChainServerAccess::set_password(const std::string &password)
{
   OSStatus status;
   if (m_itemRef == NULL)
   {
      std::string oldPassword;
      status = retrieve(oldPassword);

      if (status == errSecItemNotFound)
      {
         status = SecKeychainAddGenericPassword(
                                                NULL,                                        // default keychain
                                                static_cast<uint32_t>(m_service.length()),   // length of service name
                                                m_service.data(),                            // service name
                                                static_cast<uint32_t>(m_username.length()),  // length of account name
                                                m_username.data(),                           // account name
                                                static_cast<uint32_t>(password.length()),    // length of password
                                                password.data(),                             // pointer to password data
                                                &m_itemRef                                   // the item reference
                                                );
         if (status != noErr)
         {
            std::stringstream ss;
            ss << "Error adding password: " << status;
            log(ss.str());
         }
         
         return;
      }
   }

   status = SecKeychainItemModifyAttributesAndData(
                                                   m_itemRef,                                   // the item reference
                                                   NULL,                                        // no change to attributes
                                                   static_cast<uint32_t>(password.length()),    // length of password
                                                   password.data()                              // pointer to password data
                                                   );

   if (status != noErr)
   {
      std::stringstream ss;
      ss << "Error changing password: " << status;
      log(ss.str());
   }
}
