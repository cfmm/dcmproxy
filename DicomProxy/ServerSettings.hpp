//
//  ServerSettings.hpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-19.
//  Copyright © 2017 CFMM. All rights reserved.
//

#ifndef ServerSettings_h
#define ServerSettings_h

#ifdef __APPLE__
   #include "KeyChainServerAccess.hpp"
   typedef KeyChainServerAccess ServerSettings;
#else
   #include "LocalServerAccess.hpp"
   typedef LocalServerAccess ServerSettings;
#endif

#endif /* ServerSettings_h */
