//
//  KeyChainPasswordAccess.h
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#ifndef KeyChainServerAccess_hpp
#define KeyChainServerAccess_hpp

#include "ServerAccess.hpp"
#include <Security/Security.h>

class KeyChainServerAccess : public ServerAccess
{
public:
   KeyChainServerAccess(const std::string &hostname,
                        unsigned short port,
                        const std::string &username,
                        const std::string &service,
                        void (*log)(const std::string &msg)=nullptr)
   : ServerAccess(hostname, port, username, log)
   , m_service(service)
   , m_itemRef(NULL)
   {}

protected:
   virtual bool retrieve_password(std::string &password) const;
   virtual void set_password(const std::string &password);

private:
   OSStatus retrieve(std::string &password) const;
   
   std::string m_service;
   mutable SecKeychainItemRef m_itemRef;
};

#endif /* KeyChainServerAccess_hpp */
