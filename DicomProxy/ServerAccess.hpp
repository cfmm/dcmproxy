//
//  PasswordAccess.hpp
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#ifndef ServerAccess_hpp
#define ServerAccess_hpp

#include <string>

class ServerAccess
{
public:
   ServerAccess(const std::string &hostname,
                unsigned short port,
                const std::string &username,
                void (*log)(const std::string &msg)=nullptr)
   : m_hostname(hostname)
   , m_port(port)
   , m_username(username)
   , m_log(log)
   {}

   bool getPassword(std::string &password) const;

   const std::string &hostname() const;

   const std::string &username() const;

   unsigned short port() const;

   void configure(const std::string &hostname, unsigned short port, const std::string &username, const std::string &password);

   void password(const std::string &password);

   void log(const std::string &msg) const
   {
      if (m_log)
         m_log(msg);
   }

   virtual bool query();
protected:
   virtual bool retrieve_password(std::string &password) const =0;
   virtual void set_password(const std::string &password) =0;

   std::string m_hostname;
   unsigned short m_port;
   std::string m_username;

   void (*m_log)(const std::string &msg);

};

#endif /* ServerAccess_hpp */
