//
//  LocalServerAccess.cpp
//  DicomProxy
//
//  Created by Martyn Klassen on 2017-10-19.
//  Copyright © 2017 CFMM. All rights reserved.
//

#include "LocalServerAccess.hpp"
#ifdef WIN32
   #include <windows.h>
   #define BUFSIZE 2048
   #define VARNAME TEXT("DCMPROXY_PASSWORD")
#else
   #include <termios.h>
   #include <unistd.h>
#endif
#include <iostream>


bool LocalServerAccess::retrieve_password(std::string &password) const
{
   if (m_password.size() > 0)
   {
      password = m_password;
      return true;
   }
   return false;
}

void LocalServerAccess::set_password(std::string const &password)
{
   m_password = password;
}



void SetStdinEcho(bool enable = true)
{
#ifdef WIN32
   HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
   DWORD mode;
   GetConsoleMode(hStdin, &mode);
   
   if( !enable )
      mode &= ~ENABLE_ECHO_INPUT;
   else
      mode |= ENABLE_ECHO_INPUT;
   
   SetConsoleMode(hStdin, mode );
   
#else
   struct termios tty;
   tcgetattr(STDIN_FILENO, &tty);
   if( !enable )
      tty.c_lflag &= ~ECHO;
   else
      tty.c_lflag |= ECHO;
   
   (void) tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

bool GetEnvPassword(std::string &password)
{
#ifdef WIN32
   password.resize(0);
   
   DWORD dwRet, dwErr;
   LPTSTR pszValue;
   
   pszValue = (LPTSTR) malloc(BUFSIZE*sizeof(TCHAR));
   if(NULL == pszValue)
   {
      std::cerr << "Out of memory" << std::endl;
      return false;
   }
      
   dwRet = GetEnvironmentVariable(VARNAME, pszValue, BUFSIZE);
      
   if(0 == dwRet)
   {
      dwErr = GetLastError();
      if( ERROR_ENVVAR_NOT_FOUND == dwErr )
      {
         // Environment variable does not exist
         return false;
      }
   }
   else if(BUFSIZE < dwRet)
   {
      pszValue = (LPTSTR) realloc(pszValue, dwRet*sizeof(TCHAR));
      if(NULL == pszValue)
      {
         std::cerr << "Out of memory" << std::endl;
         return false;
      }
      dwRet = GetEnvironmentVariable(VARNAME, pszValue, dwRet);
      if(!dwRet)
      {
         std::cerr << "GetEnvironmentVariable failed (%d)" << GetLastError() << std::endl;
         return false;
      }
   }
   password = std::string(pszValue);
   return true;
   
#else
   char *pPassword;
   pPassword = getenv("DCMPROXY_PASSWORD");
   if (pPassword != NULL)
   {
      password = pPassword;
      return true;
   }
#endif
   return false;
}

bool LocalServerAccess::query()
{
   if (!GetEnvPassword(m_password) || m_password.size() < 1)
   {
      std::cout << "Password:";
      SetStdinEcho(false);
      std::cin >> m_password;
      SetStdinEcho(true);
      std::cout << std::endl;
   }
   return m_password.size() > 0;
}
