//
//  PDU.cpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-12.
//  Copyright © 2017 CFMM. All rights reserved.
//

#include "PDU.hpp"
#include <cstring>
#include <vector>

#ifdef WIN32
#include <Winsock2.h>
#else
#include <arpa/inet.h>
#endif

void copyPDU(char const **oldData, char **newData)
{
    uint16_t block = ntohs(*(reinterpret_cast<uint16_t const*>((*oldData) + 2))) + 4;
    memcpy(*newData, *oldData, block);
    (*oldData) += block;
    (*newData) += block;
}

void createUserdata(std::vector<char> &userdata, std::string const &username, std::string const &password)
{
    uint16_t length = static_cast<uint16_t>(username.length() + password.length() + 6);
    userdata.resize(length+4);
    
    userdata[0] = 0x58;
    userdata[1] = 0x00;
    *(reinterpret_cast<uint16_t *>(&(userdata[2]))) = htons(length);
    userdata[4] = 0x02;     // username and password authentication type
    userdata[5] = 0x00;     // Do not require confirmation
    *(reinterpret_cast<uint16_t *>(&(userdata[6]))) = htons(static_cast<uint16_t>(username.length()));
    memcpy(&(userdata[8]), username.c_str(), username.length());
    
    size_t offset = 8 + username.length();
    *(reinterpret_cast<uint16_t *>(&(userdata[offset]))) = htons(static_cast<uint16_t>(password.length()));
    offset+=2;
    memcpy(&(userdata[offset]), password.c_str(), password.length());
}

size_t editPDU(char *newPDU, char const *previousPDU, size_t lengthPDU, char const *userdata, size_t lengthUserdata)
{
    /*
     Association Reequest PDU
     Bytes
     1          PDU-type 0x01
     2          Reserved 0x00
     3-6        PDU-Length
     7-74       Fixed length fiels
     75-xxx     Variable fields
     
     Other Fields
     Bytes
     1          PDU-type 0x50
     2          Reserved
     3-4        PDU-Length
     5-xxx      Variable fields
     
     User Authentication
     Bytes
     1          PDU-type 0x58
     2          Reserved 0x00
     3-4        PDU-length
     5          Authetication type 0x02 Username and password
     6          Response required 0x01 No
     7-8        Username length
     9-n        Username
     n+(10-11)  Password length
     n+(12-xx)  Password
     */
    
    // Get pointers to the PDU data fields
    char const *oldData = previousPDU;
    char *newData = newPDU;
    size_t length = lengthPDU;
    
    // Complete the PDU header information to mark the PDU as an Associate RQ
    newData[0] = 0x01;
    newData[1] = 0x00;
    newData += 2;
    
    // Record the 32 bit length field location
    uint32_t *ptrPDULength = reinterpret_cast<uint32_t *>(newData);
    newData += 4;
    
    // Record the pointer to the end of the PDU data
    char const *stopField = oldData + length;
    
    // Copy the fixed length PDU fields
    memcpy(newData, oldData, 68);
    oldData += 68;
    newData += 68;
    
    // Allocate field for data block size
    size_t block;
    
    // Record the user authentication PDU has not been added
    bool noUser = true;
    
    // Loop through the variable length PDU fields
    while (oldData < stopField) {
        if (*oldData == 0x50) {
            // The other fields PDU which contains the user authentication PDU
            
            // Get the current size of the other fields PDU
            uint16_t fieldSize = ntohs(*(reinterpret_cast<uint16_t const *>(oldData + 2)));
            
            // Get the pointer to field size so it can be filled in latter
            uint16_t *ptrFieldSize = reinterpret_cast<uint16_t *>(newData + 2);
            
            // Set the PDU type in the new buffer
            newData[0] = 0x50;
            newData[1] = 0x00;
            
            // Skip the type and length fields
            oldData += 4;
            newData += 4;
            
            // Record the stopping location for the other fields PDU
            char const *stop = oldData + fieldSize;
            
            // Loop through the other fields
            while (oldData < stop) {
                if (*oldData == 0x58) {
                    // Found existing user authentication PDU
                    
                    // Copy in the new user authentication PDU
                    memcpy(newData, userdata, lengthUserdata);
                    newData += lengthUserdata;
                    
                    // Skip the existing user identification
                    block = ntohs(*(reinterpret_cast<uint16_t const *>(oldData + 2))) + 4;
                    oldData += block;
                    
                    // Record the new size of the other fields PDU
                    // It has changed by the difference in length of the new and old user authentication PDU
                    fieldSize += static_cast<int16_t>(lengthUserdata - block);
                    *ptrFieldSize = htons(fieldSize);
                    break;
                }
                else {
                    // Copy other PDUs from old to new buffer
                    copyPDU(&oldData, &newData);
                }
            }
            
            if (noUser)
            {
                // User authentication PDU was not found in other field PDU so add it to the end
                memcpy(newData, userdata, lengthUserdata);
                newData += lengthUserdata;
                
                // Record that the other fields PDU is now longer
                *ptrFieldSize = htons(static_cast<uint16_t>(fieldSize + lengthUserdata));
            }
            
            // Copy all the rest of the data in the old buffer to the new buffer
            block = length - static_cast<size_t>(oldData - previousPDU);
            memcpy(newData, oldData, block);
            newData += block;
            noUser = false;
            break;
        }
        else {
            // Copy other PDUs from old to new buffer
            copyPDU(&oldData, &newData);
        }
    }
    
    if (noUser)
    {
        // The other fields tag (0x50) was not found so the user authentication has not been added
        
        // Add the PDU type for the other fields
        newData[0] = 0x50;
        newData[1] = 0x00;
        
        // Record the length the user authentication PDU
        newData += 2;
        *(reinterpret_cast<uint16_t *>(newData)) = htons(static_cast<uint16_t>(lengthUserdata));
        
        // Copy the user authentication PDU into the output buffer
        newData += 2;
        memcpy(newData, userdata, lengthUserdata);
        newData += lengthUserdata;
    }
    
    // Calcuate the total PDU length
    length = (newData - newPDU);
    
    // Record the PDU length minus the type and length fields
    *ptrPDULength = htonl(static_cast<uint32_t>(length - 6));
    
    return length;
}
