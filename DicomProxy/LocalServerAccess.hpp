//
//  LocalServerAccess.hpp
//  DicomProxy
//
//  Created by Martyn Klassen on 2017-10-19.
//  Copyright © 2017 CFMM. All rights reserved.
//

#ifndef LocalServerAccess_hpp
#define LocalServerAccess_hpp

#include "ServerAccess.hpp"

class LocalServerAccess : public ServerAccess
{
   public:
      LocalServerAccess(std::string const &hostname,
                        unsigned short port,
                        std::string const &username,
                        std::string const&service,
                        void (*log)(const std::string &msg)=nullptr)
      : ServerAccess(hostname, port, username, log)
      , m_password()
      {}
   
      virtual bool query();
   
   protected:
      virtual bool retrieve_password(std::string &password) const;
      virtual void set_password(std::string const &password);
   
   private:
      std::string m_password;
};

#endif /* LocalServerAccess_hpp */
