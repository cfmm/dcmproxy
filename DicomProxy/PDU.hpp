//
//  PDU.hpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-12.
//  Copyright © 2017 CFMM. All rights reserved.
//

#ifndef PDU_hpp
#define PDU_hpp

#include <string>
#include <vector>
#include <stdio.h>

void createUserdata(std::vector<char> &userdata, std::string const &username, std::string const &password);
size_t editPDU(char *newPDU, char const *previousPDU, size_t lengthPDU, char const *userdata, size_t lengthUserData);

#endif /* PDU_hpp */
