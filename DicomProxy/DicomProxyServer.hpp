//
//  Server.hpp
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#ifndef DicomProxyServer_hpp
#define DicomProxyServer_hpp

#include "DicomProxyConnection.hpp"
#include "ServerAccess.hpp"


class DicomProxyServer
{
public:
   DicomProxyServer(boost::asio::io_service &io_service,
                    const std::string & bindAddress,
                    unsigned short bindPort,
                    const ServerAccess &access)
   : m_accept(io_service, ip::tcp::endpoint(boost::asio::ip::address::from_string(bindAddress), bindPort))
   , m_ctx(ssl::context::sslv23)
   , m_access(access)
   , m_maximum_retries(5)
   {
      m_ctx.set_default_verify_paths();
      start_accept();
   }

   void close()
   {
      boost::system::error_code error;
      m_accept.close(error);
   }

   unsigned int maximum_retries()
   {
      return m_maximum_retries;
   }

   void maximum_retries(unsigned int maximum_retries)
   {
      m_maximum_retries = maximum_retries;
   }
private:
   void start_accept()
   {
      DicomProxyConnection::pointer new_connection = DicomProxyConnection::create(m_accept.get_io_service(), m_ctx, m_access);

      new_connection->maximum_retries(m_maximum_retries);

      m_accept.async_accept(new_connection->socket(),
                            boost::bind(&DicomProxyServer::handle_accept, this, new_connection,
                                        boost::asio::placeholders::error));
   }

   void handle_accept(DicomProxyConnection::pointer new_connection,
                      const boost::system::error_code& error)
   {
      if (error)
      {
         std::stringstream ss;
         ss << "Error accepting client connection: " << error.value() << ": " << error.message();
         m_access.log(ss.str());
      }
      else
      {
         new_connection->start();
      }

      start_accept();
   }

   ip::tcp::acceptor m_accept;
   ssl::context m_ctx;

   unsigned int m_maximum_retries;

   const ServerAccess &m_access;
};

#endif /* DicomProxyServer_hpp */
