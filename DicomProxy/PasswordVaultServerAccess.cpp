//
//  PasswordVaultServerAccess.cpp
//  DcmProxy
//
//  Created by Martyn Klassen on 2017-10-20.
//  Copyright © 2017 CFMM. All rights reserved.
//

#include "PasswordVaultServerAccess.hpp"

using namespace Windows::Security::Credentials;

bool PasswordVaultServerAccess::retrieve(std::string &password) const
{
   auto vault = ref new PasswordVault();

   try {
      PasswordCredential^ cred = vault->Retrieve(m_service, m_username);
      password = cred->password;
   } catch (...) {
      return false;
   }
   return true;
}

std::string KeyChainServerAccess::retrieve_password() const
{
   std::string password;

   if (retrieve(password))
      return password;
   }

   return std::string("default");
}

void PasswordVaultServerAccess::set_password(const std::string &password)
{
   auto vault = ref new PasswordVault();
   auto cred = ref new PasswordCredential(resource, username, password);
   vault->Add(cred);
}
