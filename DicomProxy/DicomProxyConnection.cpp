//
//  DicomProxyConnection.cpp
//  DICOM Proxy
//
//  Created by Martyn Klassen on 2017-03-14.
//
//

#include "DicomProxyConnection.hpp"
#include "PDU.hpp"

std::string int2string(unsigned short number)
{
   std::stringstream ss;
   ss << number;
   return ss.str();
}

void DicomProxyConnection::start()
{
   // Connect the server using SSL
   boost::system::error_code error;
   ip::tcp::resolver::query query(m_access.hostname(), int2string(m_access.port()));

   boost::asio::async_connect(m_server.lowest_layer(), m_resolver.resolve(query),
                              boost::bind(&DicomProxyConnection::handle_connect, shared_from_this(),
                                          boost::asio::placeholders::error,
                                          boost::asio::placeholders::iterator));
}

void DicomProxyConnection::handle_connect(const boost::system::error_code& connect_error, ip::tcp::resolver::iterator iterator)
{
   boost::system::error_code error;

   if (connect_error == boost::asio::error::timed_out)
   {
      std::stringstream ss;
      ss << "Error connecting to server: " << connect_error.message();
      m_access.log(ss.str());

      if (++m_retries > m_maximum_retries)
      {
         m_client.close(error);

         ss.str(std::string());
         ss << "Error maximum retries (" << m_maximum_retries << ") exceeded. Closing connection.";
         m_access.log(ss.str());

         // Try again
         start();
         return;
      }
   }
   else if (connect_error)
   {
      m_client.close(error);
      
      std::stringstream ss;
      ss << "Error connecting to server: " << connect_error.message();
      m_access.log(ss.str());
      
      return;
   }

   m_server.lowest_layer().set_option(ip::tcp::no_delay(true));

   // Certificates are not verified
   m_server.set_verify_mode(ssl::verify_none);
   //m_server.set_verify_mode(ssl::verify_peer);
   //m_server.set_verify_callback(ssl::rfc2818_verification(m_access.hostname()));

   m_server.handshake(ssl_stream::client);

   size_t length;

   // Read in the type and length
   boost::asio::read(m_client, boost::asio::buffer(m_server_buffer), boost::asio::transfer_exactly(6), error);

   // Check the type
   if (m_server_buffer[0] == 0x01) {
      // This is an association request

      // Get the length
      length = ntohl(*(reinterpret_cast<uint32_t *>(&(m_server_buffer[2]))));

      std::vector<char> userdata;
      {
         const std::string username = m_access.username();
         std::string password;
         m_access.getPassword(password);
         createUserdata(userdata, username, password);
      }
      
      // The maximum length of the PDU to the server
      // Requires 6 bytes for PDU type and length
      // May need 4 bytes for other fields type and length
      // Requires userdata.size() bytes for user information
      size_t maxLength = length + 10 + userdata.size();
      if (m_server_buffer.size() < maxLength)
      {
         m_client_buffer.resize(maxLength);
         m_server_buffer.resize(maxLength);
      }

      // Read in the rest of the Association RQ PDU
      boost::asio::read(m_client, boost::asio::buffer(m_server_buffer), boost::asio::transfer_exactly(length), error);

      // Edit the PDU by inserting the user identity
      length = editPDU(m_client_buffer.data(), m_server_buffer.data(), length, userdata.data(), userdata.size());
   }
   else
   {
      // Not an association request so just transfer directly to server
      // The client_buffer is read from the client and written to the server
      // The server_buffer is read from the server and written to the client
      // Need to move the data in the server_buffer to the client_buffer for
      // writing to the server
      length = 6;
      memcpy(m_client_buffer.data(), m_server_buffer.data(), length);
   }

   // Start the client loop (client read -> server write)
   // by writing the modified Association RQ PDU to the server
   boost::asio::async_write(m_server, boost::asio::buffer(m_client_buffer, length),
                            boost::bind(&DicomProxyConnection::handle_server_write, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

   // Start the server loop (server read -> client write)
   // by reading something from the server
   m_server.async_read_some(boost::asio::buffer(m_server_buffer),
                            boost::bind(&DicomProxyConnection::handle_server_read, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}


void DicomProxyConnection::handle_client_read(const boost::system::error_code& error,
                                              size_t bytes_transferred)
{
   if (error)
   {
      // Write anything that was read from client to the server
      // But stop reading anything further from the client
      boost::system::error_code ignore_error;
      boost::asio::write(m_server, boost::asio::buffer(m_client_buffer, bytes_transferred), ignore_error);
      
      // Client has terminated connection so no further communication is required
      // Shutdown server and close client connections
      // The server can be shutdown because even if it is still sending data,
      // the client is no longer listening
      // This forces the server loop to terminate as well
      m_server.shutdown(ignore_error);
      m_client.close(ignore_error);

      if ((error != boost::asio::error::eof) &&
          (error != boost::asio::error::bad_descriptor))
      {
         // EOF is a "normal" shutdown, but anything else is an exception
         std::stringstream ss;
         ss << "Error on reading from client: " << error.value() << ": " << error.message();
         m_access.log(ss.str());
      }
      return;
   }

   // Writes all bytes_transferred from the client to the server
   boost::asio::async_write(m_server, boost::asio::buffer(m_client_buffer, bytes_transferred),
                            boost::bind(&DicomProxyConnection::handle_server_write, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

}

void DicomProxyConnection::handle_server_write(const boost::system::error_code& error,
                                               size_t /*bytes_transferred*/)
{
   // Read in a block of data from the client
   m_client.async_read_some(boost::asio::buffer(m_client_buffer),
                            boost::bind(&DicomProxyConnection::handle_client_read, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
   if (error)
   {
      // Write error on the server
      std::stringstream ss;
      ss << "Error on writing to server: " << error.value() << ": " << error.message();
      m_access.log(ss.str());
   }
}

void DicomProxyConnection::handle_server_read(const boost::system::error_code& error,
                                              size_t bytes_transferred)
{
   if (error)
   {
      // Write anything that was read from server to the client
      // But stop reading anything further from the client
      boost::system::error_code ignore_error;
      boost::asio::write(m_client, boost::asio::buffer(m_server_buffer, bytes_transferred), ignore_error);

      // Server has terminated connection so no further communication is required
      // Shutdown server and close client connections
      // The client can be closed because even if it is still sending data,
      // the server is no longer listening
      // This forces the client loop to terminate as well
      m_server.shutdown(ignore_error);
      m_client.close(ignore_error);
      
      if ((error != boost::asio::error::eof) &&
          (error != boost::asio::error::connection_reset))
      {
         // EOF is a "normal" shutdown, but anything else is an exception
         std::stringstream ss;
         ss << "Error on reading from server: " << error.value() << ": " << error.message();
         m_access.log(ss.str());
      }
      return;
   }

   // Write all bytes_transferred form the server to the client
   boost::asio::async_write(m_client, boost::asio::buffer(m_server_buffer, bytes_transferred),
                            boost::bind(&DicomProxyConnection::handle_client_write, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void DicomProxyConnection::handle_client_write(const boost::system::error_code& error,
                                               size_t /*bytes_transferred*/)
{
   // Read in a block of data from the server
   m_server.async_read_some(boost::asio::buffer(m_server_buffer),
                            boost::bind(&DicomProxyConnection::handle_server_read, shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));

   if (error)
   {
      std::stringstream ss;
      ss << "Error on writing to client: " << error.value() << ": " << error.message();
      m_access.log(ss.str());
   }
}
