//
//  main.cpp
//  AssociationProxy
//
//  Created by Martyn Klassen on 2017-02-21.
//
//

#include <iostream>
#include <string>
#include <fstream>

#ifndef WIN32
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#pragma clang diagnostic ignored "-Wcomma"
#endif

#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#ifndef WIN32
#pragma clang diagnostic pop
#endif

#include "DicomProxyServer.hpp"
#include "ServerSettings.hpp"

namespace po = boost::program_options;

int main(int argc, const char * argv[]) {
   std::string configFileName;
   std::string bindAddress;
   unsigned short bindPort;
   std::string dicomServer;
   unsigned short port;
   std::string username;
   std::string password;
   po::options_description config("Configuration");
   config.add_options()
      ("config", po::value<std::string>(&configFileName), "Name of configuration file")
      ("bind-address", po::value<std::string>(&bindAddress)->default_value("127.0.0.1"), "address on which to listen")
      ("bind-port", po::value<unsigned short>(&bindPort)->default_value(11122), "port on which to listen")
      ("server", po::value<std::string>(&dicomServer)->default_value("127.0.0.1"), "DICOM server address")
      ("port", po::value<unsigned short>(&port)->default_value(11112), "DICOM server port")
      ("username", po::value<std::string>(&username), "username with which to authenticate to DICOM server")
      ("password", po::value<std::string>(&password), "Password for user")
      ("help", "Produce help message")
      ;

   po::variables_map vm;
   po::store(po::parse_command_line(argc, argv, config), vm);
   po::notify(vm);
   
   if (vm.count("help"))
   {
      std::cout << config << std::endl;
      return EXIT_SUCCESS;
   }

   if (vm.count("config"))
   {
      std::ifstream config_file(configFileName);
      po::store(po::parse_config_file(config_file, config), vm);
      po::notify(vm);
   }

   if (vm.count("username") < 1)
   {
      std::cout << "No username set." << std::endl;
      std::cout << config << std::endl;
      return EXIT_FAILURE;
   }
   
   ServerSettings access(dicomServer, port, username, "DcmProxy");

   if (vm.count("password"))
   {
      access.password(password);
   }
   else if (!access.query())
   {
      std::cout << "No password set." << std::endl;
      std::cout << config << std::endl;
      return EXIT_FAILURE;
   }
   
   boost::asio::io_service io_service;
   DicomProxyServer server(io_service,
                           bindAddress,
                           bindPort,
                           access);
   
   // Run the server
   io_service.run();
   // Shutdown - needs to be called from another thread
   io_service.stop();
   return 0;
}
